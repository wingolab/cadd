# caddToBed

Takes the [cadd](http://cadd.gs.washington.edu/home) data 
[big cadd file v1.3](http://krishna.gs.washington.edu/download/CADD/v1.3/whole_genome_SNVs.tsv.gz) 
and processes it into a bed format with a fake stop position to prepare it for 
use with liftOver.

## The approach we are taking is:
1)	there should be no assumption that all sites have 3 entries
2)	we only report sites where the reference base is either an A, C, G, or T 
3)	we only report sites where the variant base is either an A, C, G, or T
4)	all non-conforming sites will be logged 

## The output format:

`chr pos-1 pos data...`

The rationale for this is that cadd appears 1-indexed but `liftOver`, should 
that be needed, uses a 0-based index.
