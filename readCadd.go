package cadd

import (
	"bufio"
	"crypto/sha512"
	"errors"
	"fmt"
	gzip "github.com/klauspost/pgzip"
	"io"
	"log"
	"os"
	"strings"
)

var (
	errComment    = errors.New("Line Contains Comment.")
	errRefNotBase = errors.New("Reference is not an A, C, G, T")
	errVarNotBase = errors.New("Alternate allele is not an A, C, G, T")
	errChrom      = errors.New("Chromosome not consistent for 3 consecutive sites.")
	errPos        = errors.New("Position not consistent for 3 consecutive sites.")
	errRef        = errors.New("Reference not consistent for 3 consecutive sites.")
	errLines      = errors.New("Passed lines not divisible by 3 to ReadCadd().")
	errVar        = errors.New("Variants are out of order.")
)

// the cadd output format consists of a line with a chrom, position, and a
// scaled and raw score. A particular cadd score is defined by a reference and
// input base. The goal of the processor is to group all cadd scores for a
// given genomic site, and to check the validity of the site's data - i.e., only
// a non-ambigious reference base and input base, and that the scores are floats

// approach:
// read in a line -> make a CaddEle struct -> gather consecutive CaddEle structs
// together until 1) we change position or 2) we have reached 3 sites - if we
// have changed position and have 3 sites then save the CaddEle's as a CaddSites
// and save the CaddSite

type Data struct {
	sites     []*Site
	header    []string
	siteCount int
	from      uint64
	to        uint64
}

type Site struct {
	c []*Cadd
}

type Cadd struct {
	chr              string
	pos              int
	refBase, varBase uint8
	cadd1, cadd2     float32
}

func ConvertCaddFile(file string, buff int) {
	fh, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	defer fh.Close()

	gz, err := gzip.NewReader(fh)
	if err != nil {
		log.Fatal(err)
	}
	defer gz.Close()

	r := bufio.NewReader(gz)
	ConvertCadd(r, buff)
}

func ConvertCadd(r *bufio.Reader, buff int) {

	var endOfFile bool
	var i, j, lastPos int
	var dat *Data
	var site *Site

	ch := make(chan error, 100)
	lineCount := uint64(0)

	for {
		//log.Printf("buff=%d, j=%d, i=%d, lineCount=%d\n", buff, j, i, lineCount)
		if j == 0 {
			dat = new(Data)
			dat.sites = make([]*Site, buff)
			dat.header = make([]string, 0)
			if lineCount == 0 {
				dat.from = uint64(1)
			} else {
				dat.from = lineCount
			}
		}
		if i == 0 {
			site = new(Site)
			site.c = make([]*Cadd, 3)
		}
	ReadLines:
		for {
			//log.Printf("buff=%d, j=%d, i=%d, lineCount=%d\n", buff, j, i, lineCount)
			line, err := r.ReadString('\n')
			//log.Printf("%s", line)
			lineCount++
			if err != nil {
				if err == io.EOF {
					endOfFile = true
					break
				}
				log.Fatalln(err)
			}
			if line[0] == '#' {
				dat.header = append(dat.header, line)
				continue ReadLines
			}
			c, err := NewCadd(line)
			if err != nil {
				errStr := fmt.Errorf("Error: %s, at line %d", line, lineCount)
				log.Println(errStr)
				continue ReadLines
			}
			if lastPos != 0 && lastPos != c.pos {
				//log.Printf("lastPos=%d, i=%d, c.pos=%d", lastPos, i, c.pos)
				// only one scenerio we care to keep what is in the site array
				if i == 3 {
					dat.sites[j] = site
					dat.siteCount = j
					j++
				}
				site = new(Site)
				site.c = make([]*Cadd, 3)
				i = 0
			}
			site.c[i] = c
			i++

			//log.Printf("cadd=%v, lastPos=%d\n", c, lastPos)
			lastPos = c.pos

			// write data to the file and reset the buffer
			if j == buff {
				dat.to = lineCount - 1
				go dat.PrintSites(ch)
				err := <-ch
				if err != nil {
					log.Println(err)
				}
				j = 0
				break
			}
		}
		// handle if there's still data left in the last part of the file
		if endOfFile == true {
			go dat.PrintSites(ch)
			err := <-ch
			if err != nil {
				log.Println(err)
			}
			break
		}
	}
}

func (d *Data) PrintSites(c chan error) {
	fileName := fmt.Sprintf("chunk_line.%d-%d.bed", d.from, d.to)
	f, err := os.Create(fileName)
	if err != nil {
		c <- err
	}

	for i := 0; i <= d.siteCount; i++ {
		line, err := d.sites[i].StringAndCheckSum()
		if err != nil {
			c <- err
		}
		fmt.Fprintln(f, line)
	}
	c <- nil
}

func (s *Site) StringAndCheckSum() (string, error) {
	line, err := s.String()
	if err != nil {
		return "", err
	}
	checksum, err := s.CheckSumString()
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s\t%s", line, checksum), nil
}

func (s *Site) String() (string, error) {
	str := make([]string, 3)

	var lastChr string
	var lastPos int
	var lastRef, lastVar uint8

	for i, c := range s.c {
		str[i] = fmt.Sprintf("%c=%f;%f", c.varBase, c.cadd1, c.cadd2)
		if i > 0 {
			if c.chr != lastChr {
				return "", errChrom
			}
			if c.pos != lastPos {
				return "", errPos
			}
			if c.refBase != lastRef {
				return "", errRef
			}
			if c.varBase < lastVar {
				return "", errVar
			}
		}
		lastChr = c.chr
		lastPos = c.pos
		lastRef = c.refBase
		lastVar = c.varBase
	}
	// pos (and lastPos) comes from CADD data and appears 1-indexed so we are
	// converting it to a 0-index inclusive in case we need to liftOver
	return fmt.Sprintf("chr%s\t%d\t%d\tref=%c|%s", lastChr, lastPos-1, lastPos, lastRef, strings.Join(str, "|")), nil
}

func (s *Site) CheckSumString() (string, error) {
	str, err := s.String()
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", sha512.Sum512([]byte(str))), nil
}

func NewCadd(str string) (*Cadd, error) {
	c := new(Cadd)
	if str[0] == '#' {
		return c, errComment
	}
	parsedFields, err := fmt.Sscanf(str, "%s %d %c %c %g %g", &c.chr, &c.pos, &c.refBase, &c.varBase, &c.cadd1, &c.cadd2)
	if err != nil {
		return c, err
	}

	// add check that we are using the right refBase, varBase, etc.
	// they use chromosomes: 1-22, X, Y, MT => we use: 1-22, X, Y, M
	// upshot is to change MT to M
	if parsedFields != 6 {
		return c, fmt.Errorf("Error processing fields for line '%s'", str)
	}

	// feels like a bit of a hack for working around CADD's mitochondrial chrom naming
	if c.chr == "MT" {
		c.chr = "M"
	}

	if ok := okBase(c.refBase); ok != true {
		return c, errRefNotBase
	}
	if ok := okBase(c.varBase); ok != true {
		return c, errVarNotBase
	}
	if ok := okChr(c.chr); ok != true {
		return c, fmt.Errorf("Error: Chr not recognized: '%s'", c.chr)
	}
	return c, nil
}

func okChr(str string) bool {
	switch str {
	case "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "X", "Y", "M":
		return true
	}
	return false
}

func okBase(c byte) bool {
	switch c {
	case 'A', 'C', 'G', 'T', 'a', 'c', 'g', 't':
		return true
	}
	return false
}
