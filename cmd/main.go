package main

import (
	"bitbucket.org/wingolab/cadd"
	"gopkg.in/urfave/cli.v1" // imports as package "cli"
	"os"
)

func main() {
	app := cli.NewApp()
	app.Name = "procCadd"
	app.Authors = []cli.Author{cli.Author{Name: "Thomas S. Wingo", Email: "thomas.wingo@emory.edu"}}
	app.Usage = "Convert CADD to other formats"
	app.Version = "0.0.1"
	app.Commands = []cli.Command{
		{
			Name:  "bed",
			Usage: "Convert CADD to bed-like format.",
			Flags: []cli.Flag{
				&cli.StringFlag{Name: "file", Usage: "cadd file"},
				&cli.IntFlag{Name: "buff", Usage: "Buffer size", Value: 10000000},
			},
			Action: func(c *cli.Context) {
				cadd.ConvertCaddFile(c.String("file"), c.Int("buff"))
			},
		},
		{
			Name:  "liftOver",
			Usage: "Convert lifted Over Cadd bed-like to sorted chrom files.",
			Flags: []cli.Flag{
				&cli.StringFlag{Name: "dir", Usage: "directory with cadd files"},
				&cli.StringFlag{Name: "db", Usage: "path to database file"},
			},
			Action: func(c *cli.Context) {
				cadd.ConvertLiftOverDir(c.String("dir"), c.String("db"))
			},
		},
	}
	app.Run(os.Args)
}
