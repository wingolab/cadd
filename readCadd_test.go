package cadd

import (
	"strings"
	"testing"
)

var (
	line1  = []string{"M", "1001", "A", "C", "0.1234", "1.234"}
	line2  = []string{"M", "1001", "A", "G", "-0.4321", "2.468"}
	line3  = []string{"M", "1001", "A", "T", "0.9876", "31.210"}
	okSite = []string{strings.Join(line1, "\t"),
		strings.Join(line2, "\t"),
		strings.Join(line3, "\t"),
	}
	badVarBaseOrderSite = []string{strings.Join(line2, "\t"),
		strings.Join(line3, "\t"),
		strings.Join(line1, "\t"),
	}
)

func TestNewCadd(t *testing.T) {
	c1, err := NewCadd(okSite[0])
	if err != nil {
		t.Errorf("Error making cadd site: %s", err)
	}
	if c1.chr != "M" {
		t.Errorf("Expected chr '%s', got '%s'", "M", c1.chr)
	}
	if c1.pos != 1001 {
		t.Errorf("Expected chr '%d', got '%d'", 1001, c1.pos)
	}
	if c1.refBase != uint8('A') {
		t.Errorf("Expected chr '%c', got '%c'", uint8('A'), c1.refBase)
	}
	if c1.varBase != uint8('C') {
		t.Errorf("Expected chr '%c', got '%c'", uint8('C'), c1.refBase)
	}
	if c1.cadd1 != 0.1234 {
		t.Errorf("Expected chr '%f', got '%f'", 0.1234, c1.cadd1)
	}
	if c1.cadd2 != 1.234 {
		t.Errorf("Expected chr '%f', got '%f'", 1.234, c1.cadd2)
	}
}

func TestString(t *testing.T) {
	site := make([]*Cadd, 3)
	for i, line := range okSite {
		c, err := NewCadd(line)
		if err != nil {
			t.Errorf("Error making cadd site: %s", err)
		}
		site[i] = c
	}
	siteStrs := []string{"chrM", "1000", "1001", "ref=A|C=0.123400;1.234000|G=-0.432100;2.468000|T=0.987600;31.209999"}
	siteStrExp := strings.Join(siteStrs, "\t")
	csExpStr := "702ba3633d0a587d9bcaa6d15cbe02d6a303b4768146a9964d077acfa47c9dd2bb96ba3b73dc8a9bff874ef68194584328547e47ba2c4a8542ea1fb264581945"
	fullStrExp := siteStrExp + "\t" + csExpStr

	s := Site{c: site}

	obsStr, err := s.String()
	if err != nil {
		t.Errorf("Error: %s", err)
	}
	if obsStr != siteStrExp {
		t.Errorf("Expected '%s', got '%s'", siteStrExp, obsStr)
	}

	obsStr, err = s.CheckSumString()
	if err != nil {
		t.Errorf("Error: %s", err)
	}
	if obsStr != csExpStr {
		t.Errorf("Expected '%s', got '%s'", csExpStr, obsStr)
	}

	obsStr, err = s.StringAndCheckSum()
	if err != nil {
		t.Errorf("Error: %s", err)
	}
	if obsStr != fullStrExp {
		t.Errorf("Expected '%s', got '%s'", fullStrExp, obsStr)
	}
}
