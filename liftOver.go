package cadd

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"github.com/boltdb/bolt"
	gzip "github.com/klauspost/pgzip"
	"io"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

/*
type LiftOverCadd struct {
  chr string
  pos int
  ref uint8
  []varBase uint8
  []score1 float32
  []score2 float32
}

c := new(LiftOverCadd)
varBases := make(uint8, 3)
s1 := make(float32, 3)
s2 := make(float32, 3)

parsedFields, err :=fmt.Sscanf(str, "%s\t%d\t%d\tref=%c|%c=%f;%f|%c=%f;%f|%c=%f;%f",
  &c.chr, &c.pos, &c.ref,
  &varBases[0], &s1[0], &s2[0],
  &varBases[1], &s1[1], &s2[1],
  &varBases[2], &s1[2], &s2[2], )
if err != nil {
  log.Fatal(err)
}
*/

var (
	chrPos map[string][]int
)

func init() {
	chrPos = make(map[string][]int)
}

func ConvertLiftOverDir(dir string, dbFile string) {
	db, err := bolt.Open(dbFile, 0644, nil)
	if err != nil {
		log.Fatal(err)
	}

	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}

	re := regexp.MustCompile("^chunk_line.[0-9]+-[0-9]+.bed.gz$")

	for _, file := range files {
		if re.MatchString(file.Name()) {
			ProcLiftOverFile(file.Name(), db)
		}
	}
	WriteSortedCadd(db)
}

func ProcLiftOverFile(file string, db *bolt.DB) {
	fh, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	defer fh.Close()

	gz, err := gzip.NewReader(fh)
	if err != nil {
		log.Fatal(err)
	}
	defer gz.Close()

	r := bufio.NewReader(gz)
	ProcLiftOver(r, db)
}

func ProcLiftOver(r *bufio.Reader, db *bolt.DB) {
	for {
		line, err := r.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatalln(err)
		}
		err = addLine(line, db)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func addLine(line string, db *bolt.DB) error {
	fields := strings.Split(line, "\t")
	if _, ok := chrPos[fields[0]]; !ok {
		chrPos[fields[0]] = make([]int, 0)
	}

	// convert position
	pos, err := strconv.Atoi(fields[1])
	if err != nil {
		return err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(fields[0]))
		if err != nil {
			return err
		}
		bPos := itob(pos)
		oldVal := b.Get(bPos)
		if oldVal != nil {
			return fmt.Errorf("Error: site %d has existing data - %s, new data - %s", pos, string(oldVal), line)
		}
		return b.Put(itob(pos), []byte(fields[3]))
	})
	if err != nil {
		return err
	}

	// added to running tally of sites
	chrPos[fields[0]] = append(chrPos[fields[0]], pos)
	return nil
}

// itob taken from bolddb github page
func itob(v int) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(v))
	return b
}

func WriteSortedCadd(db *bolt.DB) {
	for chr, positions := range chrPos {
		file := chr + ".bed.gz"
		fh, err := os.Create(file)
		if err != nil {
			log.Fatal(err)
		}
		defer fh.Close()

		gz := gzip.NewWriter(fh)
		defer gz.Close()
		sort.Ints(positions)
		for _, pos := range positions {
			bPos := itob(pos)
			var v []byte
			err := db.View(func(tx *bolt.Tx) error {
				b := tx.Bucket([]byte(chr))
				v = b.Get(bPos)
				return nil
			})
			if err != nil {
				log.Fatal(err)
			}
			fmt.Fprintf(gz, "%s\t%d\t%s", chr, pos, string(v))
		}
	}
}
