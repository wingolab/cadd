package cadd

import (
	"github.com/boltdb/bolt"
	"log"
	"strings"
	"testing"
)

var (
	db      *bolt.DB
	fields1 = []string{"chrM", "1000", "1001", "ref=A|C=0.123400;1.234000|G=-0.432100;2.468000|T=0.987600;31.209999", "702ba3633d0a587d9bcaa6d15cbe02d6a303b4768146a9964d077acfa47c9dd2bb96ba3b73dc8a9bff874ef68194584328547e47ba2c4a8542ea1fb264581945"}
)

func init() {
	var err error
	db, err = bolt.Open("addTest.db", 0644, nil)
	if err != nil {
		log.Fatal(err)
	}
}

func TestAddNewLine(t *testing.T) {
	line := strings.Join(fields1, "\t")

	err := addLine(line, db)
	if err != nil {
		log.Fatal(err)
	}
}

func TestWriteSortedCadd(t *testing.T) {
	WriteSortedCadd(db)
}
